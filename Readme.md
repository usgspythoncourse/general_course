## To install GIS packages
```
>python install_packages.py gis.yml
```
### to use GIS packages  
**Windows**  

```  
>activate gis  
```  

**OSX**  

```
$source activate gis  
```  
  