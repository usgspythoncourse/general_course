import numpy as np
import time

def downflow_lengths_new(A, xr, pathsteps, rinit, cinit, lengths, cellsize):

    sttime = time.time()
    B = dict()  # let's use a dictionary to hold these coordinates

    dh = 5  # delta h (noise will be added over range of -dh to +dh, so 2*dh)

    # quicker way to find distances
    X, Y = np.meshgrid([0, 1, 2], [0, 1, 2])
    dists = np.sqrt(((X-1)**2+(Y-1)**2))*cellsize

#    dists = np.array([np.sqrt(2),1,np.sqrt(2),1,1,np.sqrt(2),1,np.sqrt(2)])*cellsize

    # now loop over the simulations
    for xx in range(xr):
        Arand = (np.random.uniform(size=A.shape)*(2*dh))-dh #  %make random noise
        Ar = A+Arand #  %add random noise to dem
        r = rinit #  %get starting position
        c = cinit #

        pathr = np.zeros(int(pathsteps), dtype=int)
        pathc = np.zeros(int(pathsteps), dtype=int)
        pathr[0] = r #  %initialize list of coordinates
        pathc[0] = c

        maxlength = lengths[xx]
        flowlength = 0

        ccount = 0
        # now run simulation until it reaches ocean or hits edge of DEM, or goes
        # over max number of counts

        while ccount<pathsteps and (flowlength<maxlength) and c<Ar.shape[1]-3 and r>3 and r<Ar.shape[0]-3 and c>3:
            # nhood = np.array([[r-1,c-1],[r,c-1],[r+1,c-1],[r-1,c],[r+1,c],[r-1,c+1],[r,c+1],[r+1,c+1]])

            # we can just slice this directly
            elevs = Ar[r-1:r+2, c-1:c+2].copy() # note! teachable moment about .copy() and passing by reference
            elevs[1,1]=np.nan
            minelev = np.nanmin(elevs)

            if (minelev - Ar[r,c]) >=0: # No surrounding pixels are lower than the current one
                Ar[r,c] = minelev+0.1  # Set elevation to 10 cm higher than lowest;

            slopes = (elevs-Ar[r,c])/dists
            slopes[1,1]=0
            # find the index of the lowest slope value (note that need ravel stuff because of argmin behavior
            lowr, lowc = np.array(np.unravel_index(np.argmin(slopes), elevs.shape)) + (r-1, c-1)

            # update new position
            r=lowr
            c=lowc
            # add coordinates to path lists
            pathr[ccount]=r
            pathc[ccount]=c

            # add the new increment to flowlength
            flowlength += np.sqrt( (pathc[ccount-1] - pathc[ccount])**2 +  (pathc[ccount-1] - pathc[ccount])**2 )

            # increment the counter
            ccount += 1


        # keep only nonzero entries in pathr and pathc
        pathr = pathr[pathr!=0]
        pathc = pathc[pathc != 0]

        # retain path for this run
        B[xx]=(pathr,pathc)

    #Ac=zeros(size(Ar,1),size(Ar,2));
    Ac = np.zeros_like(Ar)

    for xx in range(xr):
        Ac[B[xx][0],B[xx][1]] = 1.0

    return Ac, time.time() - sttime