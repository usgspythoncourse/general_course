%% Create a DOWNFLOW hazard map:
% 1) Identify vents from kernel density map
% 2) Assign input parameters
% 3) Run downflow simulations from each
% 4) Calculate probabilities

clear
close all

% Send emails?
email = 0;
saveeveryvent = 0;
prefix = 'nomap_30m_spacing_xr1';

%% Vent selection:

ventspacing = 30; % in meters

% No mapping toolbox version
[fid, message]=fopen('kde_aniso_onlyeruption_mafic.txt');
headings = textscan(fid,'%s %f \r',6);
ncols = headings{2};
headings = textscan(fid,'%s %f \r',6);
nrows = headings{2};
headings = textscan(fid,'%s %f \r',6);
xllcorner = headings{2};
headings = textscan(fid,'%s %f \r',6);
yllcorner = headings{2};
headings = textscan(fid,'%s %f \r',6);
kdecellsize = headings{2};
kdex = (xllcorner+kdecellsize/2):kdecellsize:((xllcorner+kdecellsize/2)+kdecellsize*(ncols-1));
kdey = ((yllcorner+kdecellsize*(nrows-1))-kdecellsize/2):-kdecellsize:(yllcorner+kdecellsize/2);
xventi_i = (xllcorner+kdecellsize/2):ventspacing:((xllcorner+kdecellsize/2)+kdecellsize*(ncols-1));
yventi_i = ((yllcorner+kdecellsize*(nrows-1))-kdecellsize/2):-ventspacing:(yllcorner+kdecellsize/2);

%NODATA_value =headings{2}(6);
fclose(fid);
kernel = dlmread('kde_aniso_onlyeruption_mafic.txt',' ',6,0);
kernel = kernel(:,1:(end-1));
kernel(kernel==-9999)=NaN;

% Crop to AOI: from test runs
cropx = [5.66*10^5, 5.9*10^5]; 
cropy = [2.68*10^6, 2.708*10^6];
withinx = find(((xventi_i>=cropx(1))+(xventi_i<=cropx(2)))==2);
xventi = xventi_i(withinx);
withiny = find(((yventi_i>=cropy(1))+(yventi_i<=cropy(2)))==2);
yventi = yventi_i(withiny);

% Load DEM (study area): NO MAPPING TOOLBOX

[fid, message]=fopen('buffhaz_dem_30m_clip.txt');
[headings] = textscan(fid,'%s %f \r',6);
ncols = headings{2}(1);
nrows =headings{2}(2);
xllcorner =headings{2}(3);
yllcorner =headings{2}(4);
cellsize =headings{2}(5);
xi = (xllcorner+cellsize/2):cellsize:((xllcorner+cellsize/2)+cellsize*(ncols-1));
yi = ((yllcorner+cellsize*(nrows-1))-cellsize/2):-cellsize:(yllcorner+cellsize/2);

fclose(fid);
dem = dlmread('buffhaz_dem_30m_clip.txt',' ',6,0);
dem = dem(:,1:(end-1));
dem(dem==-9999)=NaN;


% Select grid of vents:

nventsi = length(xventi)*length(yventi);
ventsxi = zeros(nventsi,1);
ventsyi = zeros(nventsi,1);
ventsxi_index = zeros(nventsi,1);
ventsyi_index = zeros(nventsi,1);
vent_pi = zeros(nventsi,1);
count = 1;
for i = 1:length(xventi)
    for j = 1:length(yventi)
        % Find DEM and kde values at point:
        [c, kde_xi] = min(abs(kdex-xventi(i))); %find closest cell center
        [c, kde_yi] = min(abs(kdey-yventi(j))); %find closest cell center
        insidekde = kernel(kde_yi,kde_xi);
        
        if (((xventi(i)>max(xi))+(xventi(i)<min(xi))+(yventi(j)>max(yi))+(yventi(j)<min(yi))) > 0)
            insidedem = -1;
        else
            [c, dem_xi] = min(abs(xi-xventi(i))); %find closest cell center
            [c, dem_yi] = min(abs(yi-yventi(j))); %find closest cell center
            insidedem = dem(dem_yi,dem_xi);
        end
        if ( ( (insidedem > 0) + (insidekde > 0.00005) ) == 2) %OLD
        %if ( ( (insidedem > 0) + (insidekde > 0.0001) ) == 2)
            %disp(insidedem);
            ventsxi(count) = xventi(i);
            ventsyi(count) = yventi(j);
            ventsxi_index(count) = dem_xi;
            ventsyi_index(count) = dem_yi;
            vent_pi(count) = insidekde;
            count = count+1;
        end
    end
end

ventsx = ventsxi(1:find(ventsxi,1,'last'));
ventsy = ventsyi(1:find(ventsxi,1,'last'));
ventsx_index = ventsxi_index(1:find(ventsxi,1,'last'));
ventsy_index = ventsyi_index(1:find(ventsxi,1,'last'));
vent_p = vent_pi(1:find(ventsxi,1,'last'));

vents = [ventsy_index, ventsx_index];
vents_val = [ventsy, ventsx];

% N = 100;
% vents = size(N,2);
% for count = 1:N
%     % Find a random number
%     randval = randi([0,floor(ksum)],1);
%     total = 0;
%     i = 1;
%     while (total < randval)
%         total = total + kernel(i);
%         i = i + 1;
%     end
%     % Get coordinates
%     vents(count,1) = mod(i,size(kernel,1));
%     vents(count,2) = floor(i/size(kernel,1))+1;
%     disp(count);
% end
% 
% figure;
% imagesc(kdex,kdey,kernel); colorbar;
% set(gca,'YDir','normal');
% %pause;
% hold on;
% imagesc(xi,yi,dem>0);
% plot(vents_val(:,2),vents_val(:,1),'ok','MarkerSize',5,'MarkerFaceColor','w');
% drawnow;

clear('kernel','kernelR');

%vents = [ventsy, ventsx];
N = length(vents);

if (email == 1)
    sendmail('hdietterich@usgs.gov','vents selected');
else
    fprintf('%s\n','Vents selected');
end

%% Assign input parameters:

xr = 1; % 100 simulations for each vent
%maxdist = 22000; % Flows stop at 22 km away from the vent
pathsteps = 100000; % Iteration limit of 1 million steps

%% Set up parallel computing toolbox
%p = gcp; %parpool(32);
%disp(p.NumWorkers);

%% Run downflow simulations from each

% Set maximum distance based on length frequency:
lengths = csvread('justbasaltlengths.csv');
lengths = lengths.*1000; % Convert to meters
y=0:500:ceil(max(lengths));
mu = mean(lengths);
sigma = std(lengths);
f = exp(-(y-mu).^2./(2*sigma^2))./(sigma*sqrt(2*pi)); % Length distribution

totaltimes = zeros(N,1);
totalresults = zeros(size(dem));
if (saveeveryvent==1)  
    savebyvent = zeros(size(dem,1),size(dem,2),N);
end
progress = 0;
for ventn = 1:N
    rinit = ventsy_index(ventn);
    cinit = ventsx_index(ventn);
    
    % Make length sample:
    Lis = discretesample(f,xr);
    Lsample = y(Lis);
    %Lsample = ones(xr,1).*30000;
    
    progress = progress+1;
    fprintf('%s %d %s %d\n','Progress',progress,'out of',N);
    [results, times] = downflow_lengths_new(dem, xr, pathsteps,rinit,cinit,Lsample,cellsize);
    if (saveeveryvent==1)  
        savebyvent(:,:,ventn) = results;
    end
    
    totaltimes(ventn) = times;
    % TO DO: Multiply by probability of vent opening and length!
    totalresults = totalresults + (results.*vent_p(ventn));

end

%figure;
%imagesc(totalresults);colorbar;

%fprintf('Total time (hours): %f\n',sum(totaltimes)/3600);
%fprintf('Average time (hours): %f\n',mean(totaltimes)/3600);

if (email == 1)
    sendmail('hdietterich@usgs.gov','downflow finished');
else
    fprintf('%s\n','Downflow finished');
end

%% Save final results

expraster=totalresults; %   <= set image to export
fname=sprintf('%s_%d_%d_%d_%d%s',prefix,ventspacing,N,xr,cellsize,'.txt');  %    <= set filename

[fid, ~]=fopen(fname,'wt');

%ncols = demR.RasterSize(2);
%nrows = demR.RasterSize(1);
%xllcorner = demR.XWorldLimits(1);
%yllcorner = demR.YWorldLimits(1);
%cellsize = demR.CellExtentInWorldX;

% Write header
fprintf(fid,'%s','ncols        ');  %1
fprintf(fid,'%12.0f\n', ncols);
fprintf(fid,'%s','nrows        ');  %2
fprintf(fid,'%12.0f\n', nrows);
fprintf(fid,'%s','xllcorner    ');  %3
fprintf(fid,'%f\n', xllcorner);
fprintf(fid,'%s','yllcorner    ');  %4
fprintf(fid,'%f\n', yllcorner);
fprintf(fid,'%s','cellsize     ');  %5
fprintf(fid,'%f\n', cellsize);
fprintf(fid,'%s','NODATA_value ');  %6
fprintf(fid,'%f\n', -9999);

% Write Matrix
% substitute to NaN the NODATA_value -9999
%expraster(isnan(expraster)) = -9999;
expraster(expraster==0) = -9999; % Alternatively, set 0 values to No Data

for CurrRow = 1:nrows;   
    fprintf(fid,'%f ',expraster(CurrRow,:));
    fprintf(fid,'%s\n', ' ');
end

fclose(fid);
disp('Results file saved.');

%% Display result
displayresult = 1;
if (displayresult == 1)
    figure;
    dem(dem<0) = NaN;
    [gx,gy]=gradient(dem,cellsize,cellsize);
    xi = demR.XWorldLimits(1):cellsize:demR.XWorldLimits(2);
    yi = demR.YWorldLimits(2):-cellsize:demR.YWorldLimits(1);
    %set(gca,'ydir','normal');
    axis equal;
    j = ind2rgb(round((gx-gy)*20+45), gray);
    image(xi,yi,j);
    axis('equal');
    axis('tight');
    hold on;
    image(xi,yi,totalresults,'CDataMapping','scaled','AlphaData',(totalresults>0));
    colormap('jet');
    %title(sprintf('%.2f seconds',times(i)),'FontSize',14);
    %set(gca,'FontSize',14,'CLim',[0 hlimit],'ydir','reverse');
    qwe = colorbar;
    ylabel(qwe, 'Probability','FontSize',14)
    
    plot(vents_val(:,2),vents_val(:,1),'ok','MarkerFaceColor','r','MarkerSize',5);
    set(gca,'YDir','normal');
    
    %movfile=[pathname,runname,'/video','/frame',num2str(times(i))];
    %saveas(fig1,movfile,'png');
    %close all;
end

%% Save vents as shapefile:

fname=sprintf('%s_%d_%d_%d%s',prefix,ventspacing,N,xr,'.shp');

[geometries{1:N}] = deal('Point');

shapefile = struct('Geometry',geometries,'X',num2cell(vents_val(:,2))','Y',num2cell(vents_val(:,1))','OBJECTID',num2cell(1:N));
ventshps = shapefile';

shapewrite(ventshps,fname);

%disp('Vents file saved.');

if (email == 1)
    sendmail('hdietterich@usgs.gov','Files saved');
else
    fprintf('%s\n','Files saved');
end

%% Save .mat file for the record
fname=sprintf('%s_%d_%d_%d%s',prefix,ventspacing,N,xr,'.mat');
save(fname,'-v7.3');
