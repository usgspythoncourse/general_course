import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm as spnorm

# import the downflow lengths function
from CVO_support import downflow_lengths_new



# Set a few options
runparallel = False
prefix = 'nomap_30m_spacing_xr1'
ventspacing = 2000 # in meters
saveeveryvent = False


# make a little function to parse the ARC ASCII header
def read_ARC_ASCII_HEADER(infile):
    with open(infile, 'r') as ifp:
        line = ifp.readline().strip().split()
        ncols = int(line[1])
        line = ifp.readline().strip().split()
        nrows = int(line[1])
        line = ifp.readline().strip().split()
        xllcorner = float(line[1])
        line = ifp.readline().strip().split()
        yllcorner = float(line[1])
        line = ifp.readline().strip().split()
        cellsize = float(line[1])
        line = ifp.readline().strip().split()
        NODATA_value = float(line[1])
        return nrows,ncols,xllcorner,yllcorner,cellsize,NODATA_value


# read in the ARC ASCII KDE file
nrows,ncols,xllcorner,yllcorner,kdecellsize,NODATA_value = read_ARC_ASCII_HEADER('kde_aniso_onlyeruption_mafic.txt')
kernel = np.loadtxt('kde_aniso_onlyeruption_mafic.txt', skiprows=6)
kernel[kernel==NODATA_value] = np.nan

kdex = np.arange((xllcorner+kdecellsize/2),((xllcorner+kdecellsize/2)+kdecellsize*(ncols-1)),kdecellsize)
kdey = np.arange(((yllcorner+kdecellsize*(nrows-1))-kdecellsize/2),(yllcorner+kdecellsize/2),-kdecellsize)
xventi_i = np.arange((xllcorner+kdecellsize/2),((xllcorner+kdecellsize/2)+kdecellsize*(ncols-1)),ventspacing)
yventi_i = np.arange(((yllcorner+kdecellsize*(nrows-1))-kdecellsize/2),(yllcorner+kdecellsize/2),-ventspacing)




outfig = plt.figure(figsize=(8,8))
plt.imshow(kernel, interpolation='nearest')
plt.colorbar()
plt.title('KDE')
plt.savefig('kernel.pdf')
plt.close('all')

# ### Crop to area of interest
cropx = np.array([5.66*10**5, 5.9*10**5])
cropy = np.array([2.68*10**6, 2.708*10**6])

# cast boolean to binary (1,0) by multiplying the conditional by 1
withinx = np.where((xventi_i>=cropx[0])*1+((xventi_i<=cropx[1])*1)==2)
xventi = xventi_i[withinx];
withiny = np.where(((yventi_i>=cropy[0])*1+(yventi_i<=cropy[1])*1)==2);
yventi = yventi_i[withiny]


# ### Load up the DEM
nrows,ncols,xllcorner,yllcorner,cellsize,NODATA_value = read_ARC_ASCII_HEADER('buffhaz_dem_30m_clip.txt')


xi = np.arange((xllcorner+cellsize/2), (xllcorner+cellsize/2)+cellsize*(ncols-1), cellsize)
yi = np.arange((yllcorner+cellsize*(nrows-1)-cellsize/2), (yllcorner+cellsize/2), -cellsize)

dem = np.loadtxt('buffhaz_dem_30m_clip.txt', skiprows=6)
dem[dem==NODATA_value] = np.nan


outfig = plt.figure(figsize=(8,8))
plt.imshow(dem, interpolation='nearest')
plt.colorbar()
plt.title('DEM')
plt.savefig('DEM.pdf')
plt.close('all')


# # Select Grid of Vents

nventsi = len(xventi)*len(yventi);
ventsxi = np.zeros((nventsi));
ventsyi = np.zeros((nventsi));
ventsxi_index = np.zeros((nventsi));
ventsyi_index = np.zeros((nventsi));
vent_pi = np.zeros((nventsi));


ccount=0
ktol = 0.00005 # tolerance for whether kernel is active
maxxi = np.max(xi)
minxi = np.min(xi)
maxyi = np.max(yi)
minyi = np.min(yi)


for cx in xventi:
    for cy in yventi:
        ccount+=1
        kde_xi = np.argmin(np.abs(kdex-cx)) #find closest cell center
        kde_yi = np.argmin(np.abs(kdey-cy)) #find closest cell center
        insidekde = kernel[kde_yi,kde_xi] 
        insidedem = 1
        if cx>maxxi or cx<minxi or cy>maxyi or cy<minyi:
            insidedem -= 1
        else:
            dem_xi = np.argmin(np.abs(xi-cx))
            dem_yi = np.argmin(np.abs(yi-cy))

        if insidedem>0 and insidekde > ktol:
            ventsxi[ccount] = cx
            ventsyi[ccount] = cy
            ventsxi_index[ccount] = dem_xi;
            ventsyi_index[ccount] = dem_yi;
            vent_pi[ccount] = insidekde
            
keep_inds=np.where(ventsxi*ventsyi != 0) # multiplying will make array zero if either coordinate == 0


ventsx = ventsxi[keep_inds].astype(int)
ventsy = ventsyi[keep_inds].astype(int)
ventsx_index = ventsxi_index[keep_inds].astype(int)
ventsy_index = ventsyi_index[keep_inds].astype(int)
vent_p = vent_pi[keep_inds]

vents = np.vstack((ventsy_index,ventsx_index)).T
vents_val = np.vstack((ventsy,ventsx)).T

N=len(vents)
xr = 1 # 100 simulations for each vent
maxdist = 22000 # Flows stop at 22 km away from the vent
pathsteps = 1e6 # Iteration limit of 1 million steps


lengths = np.loadtxt('justbasaltlengths.csv')*1000 # read in and convert to meters

y = np.arange(0,np.ceil(np.max(lengths)),500)


mu=np.mean(lengths)
sig = np.std(lengths)
#f = np.exp(-(y-mu)**2/(2*sig**2))/(sig*np.sqrt(2*np.pi)) # Length distribution
f = spnorm.pdf(y,mu,sig) # use the built-in function from scipy
f /= f.sum() # need to renormalize the probabilities so that they sum to unity

outfig = plt.figure()
plt.plot(y,f)
plt.savefig('LengthDistribution.pdf')
plt.close('all')

# preallocate results arrays
totaltimes = np.zeros(N)
totalresults = np.zeros_like(dem)
if saveeveryvent:
    savebyvent = np.zeros((dem.shape[0],dem.shape[1],N));

# loop through the vents to find the downstream paths
for ventn,(cinit,rinit) in enumerate(zip(ventsx_index,ventsy_index)):
    # by using enumerate, we get our counter for free (called "ventn" in this case)
    print ("rockin' sample {0} out of {1}".format(ventn, len(ventsx_index)), end='\r')

    Lsample = np.random.choice(y, xr, p=f) # note that we can complete the discrete sample all in one step
    results, times = downflow_lengths_new(dem, xr, pathsteps,rinit,cinit,Lsample,cellsize);
    if saveeveryvent:
        savebyvent[:,:,ventn] = results

    totaltimes[ventn] = times
    # TO DO: Multiply by probability of vent opening and length!
    totalresults += (results*vent_p[ventn])

# plot and save down the results
outfig = plt.figure(figsize=(8,8))
plt.imshow(totalresults, interpolation='nearest')
plt.colorbar()
plt.title('Results')
plt.savefig('Results.pdf')
plt.close('all')

# save the results to a text file
np.savetxt('totalresults.dat', totalresults)

np.savetxt('alltimes.dat', totaltimes)
plt.hist(totaltimes, bins=25)
plt.xlabel('Runtime in Seconds')
plt.ylabel('Frequency')
plt.title('Run Times')
plt.savefig('alltimes.pdf')