function [result, times] = downflow_lengths_new(A, xr, pathsteps, rinit, cinit, lengths, cellsize)
% DOWNFLOW version after Pisa visit June 2016

tic
    
B=cell(xr,2);  %list of coordinates for each run
dh=5; %delta h (noise will be added over range of -dh to +dh, so 2*dh)

dists = [sqrt(2);1;sqrt(2);1;1;sqrt(2);1;sqrt(2)].*cellsize; % Distances within neighborhood

%now loop through simulations
for xx=1:xr
    %disp(xx);
    Arand=(rand(size(A,1),size(A,2))*(2*dh))-dh;  %make random noise
    Ar=A+Arand;  %add random noise to dem
    r=rinit;  %get starting position
    c=cinit;

    pathr=zeros(1,pathsteps);
    pathc=zeros(1,pathsteps);
    pathr(1)=r;  %initialize list of coordinates
    pathc(1)=c;
    
    maxlength = lengths(xx);
    flowlength = 0;

    count=1; % Keep vent location as first point
    %now run simulation until it reaches ocean or hits edge of DEM, or goes
    %over max number of counts
    while count<pathsteps && (flowlength<maxlength) && c<size(Ar,2)-3 && r>3 && r<size(Ar,1)-3 && c>3
        count=count+1;

        % Without the loops:
        nhood = [r-1,c-1;r,c-1;r+1,c-1;r-1,c;r+1,c;r-1,c+1;r,c+1;r+1,c+1];
        idx = sub2ind(size(Ar), nhood(:,1), nhood(:,2));
        elevs = Ar(idx);
        minelev = min(elevs);
         % No surrounding pixels are lower than the current one
            Ar(r,c) = Ar(r,c)+(minelev-Ar(r,c))+0.1; % Set elevation to 10 cm higher than lowest;           
        end
        slopes = (elevs-Ar(r,c))./dists;
        [minslope, lowestSi] = min(slopes);
        lowr = nhood(lowestSi,1);
        lowc = nhood(lowestSi,2);   

        r=lowr; %update new position
        c=lowc;
        pathr(count)=r; %add coordinates to list
        pathc(count)=c;
        flowlength = sum(sqrt(diff([pathc(1:1000:count),pathc(count)]).^2+diff([pathr(1:1000:count),pathr(count)]).^2))*cellsize;
    end
    pathr(pathr==0)=[];
    pathc(pathc==0)=[];
    B{xx,1}=pathr;  %add list to cell array
    B{xx,2}=pathc;

end

Ac=zeros(size(Ar,1),size(Ar,2));
for xx=1:xr
    thisr=B{xx,1}';
    thisc=B{xx,2}';
    Aa=[thisr thisc];
    %don't double count if a given run hits pixel more than once
    Aa=unique(Aa,'rows'); 
    for j=1:size(Aa,1)
        r=Aa(j,1);
        c=Aa(j,2);
        Ac(r,c)=Ac(r,c)+1;
    end
end

result = Ac;
times = toc;

