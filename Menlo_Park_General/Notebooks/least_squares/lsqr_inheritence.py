
# Note that if we define the model class in another file we can import it
import model
import matplotlib.pyplot as plt
import numpy as np

# then we can inherit from it
class lsqr(model.model):
    '''
    inherit from general model object
    '''

    def plot_raw_data(self):
        '''
        plot the raw data and show it on the screen
        :return:
        '''
        plt.scatter(self.observations.x, self.observations.y)
        plt.show()

    def construct_lsqr(self):
        '''
        set up the solution for the least-squares problem
        :return:
        '''
        self.y = self.observations.y.values
        self.X = np.ones((len(self.y), 2))
        self.X[:, 1] = self.observations.x.values

    def solve_lsqr(self):
        '''
        solve the least squares problem
        '''
        X = self.X
        y = self.y
        # one way, more like functions
#        self.beta_hat = np.dot(np.dot(np.linalg.inv(np.dot(X.T,X)),X.T),y)

        # another way - more MATLAB-y also method style and more readable
#        self.beta_hat = np.linalg.inv(X.T.dot(X)).dot(X.T).dot(y)

        # finally, might be good to construct each term individually
        XtX = np.dot(X.T,X)
        XtXinv = np.linalg.inv(XtX)
        XtXinvXt = np.dot(XtXinv, X.T)
        self.beta_hat = np.dot(XtXinvXt,y)

        self.y_hat = X.dot(self.beta_hat)

    def plot_results(self):
        plt.scatter(self.observations.x, self.observations.y, c='b')
        plt.plot(self.observations.x, self.y_hat, lw=2, marker='o',
                 linestyle='-', c='r')
        plt.title('Data with fit')
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.legend(['y_hat','y'])
        plt.savefig('solution.png')


if __name__ == '__main__':
    infile = 'data.xlsx'

    testprob = lsqr(infile)

    testprob.read_in_data()

#    testprob.plot_raw_data()

    testprob.construct_lsqr()

    testprob.solve_lsqr()

    testprob.plot_results()