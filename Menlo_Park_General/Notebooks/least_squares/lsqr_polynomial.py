from lsqr_inheritence import lsqr
import numpy as np


class polynomial(lsqr):
    '''
    we want to add to the __init__ constructor, so we
    call __init__ from the parent and then define an
    extra attribute for the daughter

    we can also override the lsqr construction

    all the other functions will happily be inherited from lsqr
    '''
    def __init__(self, infile, order):
        lsqr.__init__(self, infile)
        self.poly_order = order

    def construct_lsqr_higher_order(self):
        '''
        set up the solution for the least-squares problem
        allowing for higher order polynomial solution
        '''
        self.y = self.observations.y.values
        self.X = np.ones((len(self.y), self.poly_order+1))
        # now for the polynomial terms
        for j in range(1,self.poly_order+1):
            self.X[:, j] = self.observations.x.values**j

if __name__ == '__main__':
    infile = 'data.xlsx'
    order = 6

    testprob = polynomial(infile, order)

    testprob.read_in_data()

    testprob.construct_lsqr_higher_order()

    testprob.solve_lsqr()

    testprob.plot_results()