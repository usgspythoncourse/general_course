import pandas as pd
import numpy as np
class model(object):


    """
    Class to handle scattered data, fit a function, and plot results
    """

    def __init__(self, infile):
        '''
        just assign the input file and instantiate a couple properties
        :param infile: filename containing data
        '''

        self.infile = infile
        self.y_hat = None
        self.beta_hat = None
        print('Your model object is ready to rock!')

    def read_in_data(self):
        '''
        read in the data
        :return:
        '''
        self.observations = pd.read_excel(self.infile)
        print('read in data from {0}'.format(self.infile))