{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Groundwater Modeling and Python\n",
    "\n",
    "## NumPy\n",
    "This notebook demonstrates how to import and use the Numpy module to work with arrays. The [documentation for Numpy](http://www.numpy.org/ \"Numpy Documentation\") contains everything you need to know, but we will focus on some of the things that are useful for groundwater modeling tasks.  The NumPy library includes (among other things) ways of storing and manipulating data that are more efficient than standard Python arrays. Using NumPy with numerical data is much faster than using Python lists or tuples.\n",
    "\n",
    "### Outline\n",
    "* Loading Numpy\n",
    "* Creating a Numpy Array\n",
    "* Array Operations\n",
    "* Accessing Array Elements\n",
    "* Reshaping and Stacking Arrays\n",
    "* Record Arrays\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This line configures matplotlib to show figures embedded in the notebook, \n",
    "# instead of opening a new window for each figure. More about that later. \n",
    "%matplotlib inline\n",
    "import os\n",
    "from __future__ import print_function\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "#this line sets up the directory paths that we will be using\n",
    "datapath = os.path.join('.', 'data')\n",
    "print('Data will be loaded from the following directory: ', datapath)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading Numpy\n",
    "The first step is to load the numpy module.  The common approach for loading numpy is to rename it as \"np\" as part of the import statement as follows.\n",
    "\n",
    "Note that at any time, you can enter the name of an object or method and the \"?\" to see help.  Also remember that shift-tab will bring help."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Many packages, including Numpy, have a `__version__` attribute.  It is a good idea to be aware of what package version you are using."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#To see the version of numpy that was loaded, enter the following command\n",
    "print('I am running numpy version: ', np.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating a NumPy Array\n",
    "\n",
    "There are several way to create a numpy array (called an ndarray). These include:\n",
    "1. Convert a list or tuple into an array\n",
    "2. Use a numpy function\n",
    "3. Read from a file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating a Numpy Array from a List\n",
    "The following is an example of converting a list into a numpy array using the numpy [array](http://wiki.scipy.org/Numpy_Example_List#array) function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#create a new list\n",
    "lst = [1, 3, 5, 7]\n",
    "\n",
    "#convert the list into a new array\n",
    "a = np.array(lst)\n",
    "\n",
    "#print some information about the new array\n",
    "print('The array is: ', a)\n",
    "print('The type of an array is: ', type(a))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Note that the following does not work.  Brackets are required so that the input is a list.\n",
    "#a = np.array(1, 2, 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Every array contains additional information that is stored as an attribute as part of the array.  These attributes include the dimension (ndim) and the array shape (shape), for example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The number of dimensions for a: ', a.ndim)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The shape of a: ', a.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The type of a: ', a.dtype)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A two-dimension array can be created by providing two lists within brackets to the numpy array function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#create a new list\n",
    "lst1 = [1, 3, 5, 7]\n",
    "lst2 = [9, 12, 14, 32]\n",
    "\n",
    "#convert the list into a new array\n",
    "a = np.array([lst1, lst2])\n",
    "\n",
    "#print some information about the new array\n",
    "print('The array is: ')\n",
    "print(a)\n",
    "print('The type of the array is: ', type(a))\n",
    "print('The dimension of the array is: ', a.ndim)\n",
    "print('The shape of the array is: ', a.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We could also skip the step of creating two lists and just put them right into the np.array function as follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.array([[1, 3, 5, 7],[9, 12, 14, 32]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Array Generating Functions\n",
    "\n",
    "There are many different functions for creating numpy arrays.  They are described [here](http://docs.scipy.org/doc/numpy/reference/routines.array-creation.html).  \n",
    "\n",
    "Here are a few common array generating functions with links to their descriptions:\n",
    "\n",
    "* [empty](http://wiki.scipy.org/Numpy_Example_List#empty)\n",
    "* [zeros](http://wiki.scipy.org/Numpy_Example_List#zeros)\n",
    "* [ones](http://wiki.scipy.org/Numpy_Example_List#ones)\n",
    "* [arange](http://wiki.scipy.org/Numpy_Example_List#arange)\n",
    "* [linspace](http://wiki.scipy.org/Numpy_Example_List#linspace)\n",
    "\n",
    "empty, zeros, and ones will all create a new array, and they all have the same syntax.  The only difference is the value that will be assigned to all of the array elements upon initialization.  The syntax for creating an empty array is:\n",
    "\n",
    "    a = np.empty( (shape), dtype )\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create an empty array\n",
    "print(np.empty((2,2), dtype=np.int))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You are probably asking, what is this junk?  When you use `np.empty`, the only guarantee is that none of the values will be initialized.  You'll get whatever garbage was in that memory address.  You will only want to use `np.empty` if you know that you will be filling the array with meaningful values.\n",
    "\n",
    "The `dtype` is the type of array to create.  Some common examples are int (INTEGER), float (a floating point, or REAL for you FORTRAN people).  Numpy also has some of its own, like np.int, np.float, np.float32, np.float64, etc.\n",
    "\n",
    "Here is an empty float array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.empty((3,3), np.float32)\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you might expect, `np.zeros` and `np.ones` make arrays with values initialized one zero and one, respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(np.ones((3,3,3), int))\n",
    "print(np.zeros((2,2,2), float))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Input and Output\n",
    "\n",
    "Numpy has a great set of tools for saving arrays to files and reading arrays from files.  Details on these tools are [here](http://docs.scipy.org/doc/numpy/reference/routines.io.html).\n",
    "\n",
    "The following are the common ones:\n",
    "\n",
    "* [loadtxt](http://docs.scipy.org/doc/numpy/reference/generated/numpy.loadtxt.html#numpy.loadtxt)\n",
    "* [savetxt](http://docs.scipy.org/doc/numpy/reference/generated/numpy.savetxt.html#numpy.savetxt)\n",
    "* [genfromtxt](http://docs.scipy.org/doc/numpy/reference/generated/numpy.genfromtxt.html#numpy.genfromtxt)\n",
    "\n",
    "We will now test these functions using data in the `datapath/04_numpy` directory.  In this folder, there is a file called `bottom.dat`.  This file contains bottom elevations for a model with 142 rows and 113 columns.  We can read this array using the `loadtxt` function as follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filepath = os.path.join(datapath, '04_Numpy')\n",
    "filename = os.path.join(filepath, 'bottom.dat')\n",
    "bot = np.loadtxt(filename, dtype=np.float32)\n",
    "print(bot.shape, bot.dtype)\n",
    "filename = ''  #this is to prevent you from accidently writing over this file later"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can very quickly plot a two-dimensional numpy array using the imshow function, which is part of matplotlib.  Matplotlib will be covered in a subsequent notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(bot, interpolation='nearest')\n",
    "cb = plt.colorbar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing your Skills\n",
    "\n",
    "1. Write the bottom array (bot) to a new file called `mynewbot.dat` in the same folder as `bottom.dat`.\n",
    "2. Take a look at `bottom_commented.dat` in a text editor.  What about this file?  Can you read this file using loadtxt without having to manually change the file?  Hint: look at the loadtxt arguments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Skill Test 1\n",
    "#use np.savetxt here to write bot to mynewbot.dat in the same folder (datapath)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Skill Test 2\n",
    "filepath = os.path.join(datapath, '04_Numpy')\n",
    "filename = os.path.join(filepath, 'bottom_commented.dat')\n",
    "\n",
    "# fill this in\n",
    "#bot2 = \n",
    "#print bot2.shape, bot2.dtype"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Array Operations\n",
    "\n",
    "As you might expect, it is very easy to perform arithmetic operations on arrays.  So we can easily add and subtract arrays and use them in numpy functions.  The following is a couple examples of using simple arithmetic array operations as when as an example for plotting a sin function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#add\n",
    "a = np.ones((100, 100))\n",
    "a = a + 1\n",
    "print(a)\n",
    "\n",
    "#powers, division\n",
    "a = a ** 2.73 / 75\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is so much you can do with numpy arrays and the available functions.  For a list of the mathematical functions look [here](http://docs.scipy.org/doc/numpy/reference/routines.math.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#or how about a sin function\n",
    "x = np.linspace(0, 2*np.pi)\n",
    "y = np.sin(x)\n",
    "print(x, y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# An x, y plot can be quickly generated using plt.plot()\n",
    "plt.plot(x, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accessing Array Elements\n",
    "\n",
    "Okay, now for the good stuff...\n",
    "\n",
    "Numpy arrays are zero based, which means the first position is referenced as 0.  If you are a FORTRAN person or a MATLAB person, THIS WILL CAUSE YOU GREAT GRIEF!  You will be burned by the fact that the first element in an array is not `a[1]`.  It is `a[0]`.  You will accept it, eventually, and then you will find zero-based arrays to be more intuitive, maybe.\n",
    "\n",
    "Let's do some array slicing to see how all of this works."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#create an array with 10 equally spaced values\n",
    "a = np.linspace(50, 60, 10)\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#we can access the first value as\n",
    "print('The first value is: ', a[0])\n",
    "\n",
    "#the last value is\n",
    "print('The last value is: ', a[6])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Okay, this is all good. Now there is another cool part of numpy arrays, and that is that we can use negative index numbers.  So instead of a[6], we could also say a[-1]."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The last value is: ', a[-1])\n",
    "\n",
    "#hmm.  Does that mean?  yup.\n",
    "print('The second to last value is: ', a[-2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Accessing Parts of an Array\n",
    "\n",
    "So how do we access parts of an array?  The formal syntax for a one dimensional array is:\n",
    "\n",
    "    array[idxstart : idxstop : idxstride]\n",
    "    \n",
    "where idxstart is the zero-based array position to start; idxstop is the zero-based index to go up to, BUT NOT INCLUDE, and idxstride is the number of values to skip.  The following are valid array slides:\n",
    "\n",
    "    a[:]    #all elements\n",
    "    a[0:-1] #all elements\n",
    "    a[2:10] #elements in the third position up to elements in the 10th position\n",
    "    a[::2]  #start in position 0 and include every other element\n",
    "    a[::-1] #start at the end and reverse the order\n",
    "\n",
    "The following are some examples.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#make a fresh array with 100 values\n",
    "a = np.arange(100)\n",
    "print('a=', a)\n",
    "\n",
    "#print values from the beginning up to but not including the 10th value\n",
    "print('a[10]: ', a[:10])\n",
    "\n",
    "#or print the values from postion 90 to the end\n",
    "print('a[90:]', a[90:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is all good, but what if want to skip over values.  Then we can also enter a stride. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print every second value\n",
    "print(a[::2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print every 5th value from 50 to 100\n",
    "print(a[50::5])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And this all works for multi-dimensional arrays as well.  So we can access parts of a multi-dimensional array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.ones((10, 10), dtype=np.int)\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#now lets set the upper left quadrant of a to 2\n",
    "a[:5, :5] = 2\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Accessing Array Elements Using an Index List\n",
    "\n",
    "If we know the array indices that we want to access, we can specify them in a list and pass that list to the array through the brackets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.random.random(10)\n",
    "print(a)\n",
    "print('values in 2, 3, and 7 positions', a[[2, 3, 7]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#this also works for multi dimensions\n",
    "icoord = np.arange(10)\n",
    "jcoord = icoord\n",
    "a = np.zeros((10, 10), dtype='int')\n",
    "a[icoord, jcoord] = 1\n",
    "print(a)  #hey, its the identity matrix!\n",
    "\n",
    "#what do you think?  Do you think numpy has an identity function?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reshaping and Stacking Arrays\n",
    "\n",
    "Reshaping of arrays is easy to do with the np.reshape function.  The function works by taking an array and desired shape, and then returning a new array with that shape.  Note, however, that the requested shape must be consistent with the array that is passed in.  The syntax is:\n",
    "\n",
    "    np.reshape(a, (newshape))\n",
    "\n",
    "Arrays also have a reshape method, which means you can use the following form:\n",
    "\n",
    "    a.reshape( (newshape) )\n",
    "    \n",
    "Example follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.arange(100)\n",
    "print('a: ', a)\n",
    "b = np.reshape(a, (10,10))\n",
    "print('b: ', b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## hstack and vstack\n",
    "\n",
    "hstack and vstack can be used to add columns to an array or rows to an array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#vstack example -- add a row to an array\n",
    "a = np.zeros((3, 3), dtype=float)\n",
    "print(a)\n",
    "b = np.vstack((a, np.array([2, 2, 2])))\n",
    "print(b)\n",
    "print('The shape of b is ', b.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing your Skills\n",
    "\n",
    "1. Can you create a two-dimensional checkerboard array of 1's and 0's?  Can you do it with a single line of python code?\n",
    "2. In an earlier exercise, you made x and y using the following lines of code.  Now use vstack to add another row to y that has the cosing of x.  Then plot them both on the same figure.\n",
    "\n",
    "    x = np.linspace(0, 2*np.pi)\n",
    "    \n",
    "    y = np.sin(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Skill Test 1 -- checkerboard"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Skill Test 2 -- hstack\n",
    "x = np.linspace(0, 2*np.pi)\n",
    "y = np.sin(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Masking an Array\n",
    "\n",
    "Masking is the process of performing array operations on a specific set of cells, called a mask.  A mask is a logical array with the same shape as the array it is applied to.  Here is an example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#create a 10 by 10 array\n",
    "a = np.arange(100).reshape((10,10))\n",
    "print('a:\\n', a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('mask array for a == 1:\\n', a[:, :] == 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We can create a mask array as follows\n",
    "# enclosing the conditional statement in parentheses is added for clarity\n",
    "mask = (a[:,:]==1)\n",
    "print(mask)\n",
    "\n",
    "#Now we can use the mask to set values\n",
    "a[mask] = 100\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Multi-Conditional Masking\n",
    "\n",
    "For more complicated masks, numpy has the `logical_and`, `logical_or` and `logical_not` functions.  Here is an example of `logical_and`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mask = np.logical_and(a[:,:]>50, a[:,:]<75, a[:,:]==100)\n",
    "a[mask] = 100\n",
    "print(a)\n",
    "plt.imshow(a, interpolation='nearest')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numpy Where Function\n",
    "\n",
    "The numpy `where` function can be used in one of two ways.  The first way is to return array indices for array elements that meet a condition.  When used in this form, the syntax is:\n",
    "\n",
    "    where(condition)\n",
    "\n",
    "The result is a tuple of size array.ndim, that has an indices for each dimension.\n",
    "\n",
    "The `where` function can also be used with the following form:\n",
    "\n",
    "    where(condition, [x, y])\n",
    "    \n",
    "In this case, the where function returns an array that has the value of x where the condition is true and the value of y where the condition is false."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#create an array\n",
    "a = np.arange(100).reshape((10,10))\n",
    "\n",
    "#use where with the first form in which only a condition is specified\n",
    "result = np.where(a >90)\n",
    "print('result: ', result)\n",
    "\n",
    "#now we can use this result tuple for array assignments\n",
    "a[result] = -999\n",
    "print('a: ', a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#use where with condition and 'x' and 'y'\n",
    "#let's change all those -999's to -998\n",
    "np.where(a == -999, a + 1, a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing your Skills\n",
    "\n",
    "1. Experiment with the mask and where functionality in numpy.  Create an array of shape (100, 100) that has random values between 0 and 1000.  Then replace all values less than 250 with np.nan and all values greater than 750 with np.nan.  Then plot it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Test your skills!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Record Arrays\n",
    "\n",
    "Record arrays, also known as structured arrays, are a great way to store columns of data that have different types.  They can be created by specifying a dtype that defines the columns.  For example:\n",
    "\n",
    "    dtype=[('x', int), ('y', float), ('z', int)]) \n",
    "    \n",
    "Now, let's use this dtype to create an array with ones in it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dtype=[('x', int), ('y', float), ('z', int)]\n",
    "a = np.ones( (10), dtype=dtype)\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#we can access each column by using the column names\n",
    "print(a['x'])\n",
    "print(a['y'])\n",
    "print(a['z'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#we can also change how we view the record array so that we can access columns more easily\n",
    "a = a.view(np.recarray)\n",
    "print(a.x)\n",
    "print(a.y)\n",
    "print(a.z)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#to add a column, use vstack!\n",
    "dt = [('name', 'S15')]\n",
    "string_column = np.empty((10), dtype=dt).view(np.recarray)\n",
    "string_column.name = 'text string'\n",
    "\n",
    "#we can use a merge_arrays function to add a column\n",
    "import numpy.lib.recfunctions as rfn\n",
    "b = rfn.merge_arrays((a, string_column), flatten = True, usemask = False)\n",
    "\n",
    "#we need to convert b to a recarray to access individual columns as before\n",
    "b = b.view(np.recarray)\n",
    "print('Now we have appended string_column to a: \\n', b)\n",
    "print('b is of shape: ', b.shape)\n",
    "print(b.dtype.names)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing your Skills\n",
    "\n",
    "1. Become familiar with recarrays by creating one that has four columns and 100 rows.  In the first column, put an integer that starts at 99 and decreases to 0.  In the second column, put a text string that has 'row n', where n is the actual row value.  In the third column put a random number, and in the fourth column, put your name.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#do the exercise here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## genfromtxt and Record Arrays\n",
    "\n",
    "genfromtxt can automatically create a record array if the following arguments are provided (dtype=None, names=True).  When these arguments are set this way, genfromtxt will automatically determine the type for each column and will use the first non-commented line of the data file as column labels.  \n",
    "\n",
    "In the 03_numpy folder, there is a csv file called ahf.csv.  This file contains airborne helicopter measurements of elevation taken at points in the everglades.  This data was downloaded from: http://sofia.usgs.gov/exchange/desmond/desmondelev.html.\n",
    "\n",
    "In the following cell, we use genfromtxt to load this csv file into a recarray.  The beauty of this is that genfromtxt automatically determines the type of information in each columns and fills the array accordingly.  When this is done correctly, we can access each column in the array using the column text identifier as follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#specify the file name\n",
    "fname = os.path.join(datapath, '04_Numpy', 'ahf.csv')\n",
    "print('Data will be loaded from: ', fname)\n",
    "\n",
    "#load the array\n",
    "a = np.genfromtxt(fname, dtype=None, names=True, delimiter=',')\n",
    "\n",
    "#print information about the array\n",
    "print('dypte contains the column information: ', a.dtype)\n",
    "print()\n",
    "print('this is the array: ', a)\n",
    "print()\n",
    "print('this is just the column names: ', a.dtype.names)\n",
    "print()\n",
    "\n",
    "#now we can create scatter plot of the points very easily using the plt.scatter command\n",
    "#the nice thing here is that we access the array columns directly using the column names\n",
    "plt.scatter(a['X_UTM'], a['Y_UTM'], c=a['ELEV_M'], s=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading a Wrapped MODFLOW Array\n",
    "\n",
    "It is unlikely that there will be time for this, but the following function is a quick hack that can be used to read a MODFLOW external array where the columns are wrapped.  Note that you cannot use `np.genfromtxt` or `np.loadtxt` to do this if the columns are wrapped.\n",
    "\n",
    "Keep this little function in your back pocket in case you need it one day.  If you have time, read bottom.txt in the 04_numpy directory and plot it.  This array has 142 rows and 113 columns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This is a little goodie that you might need one day.\n",
    "def readarray(fname, nrow, ncol):\n",
    "    f = open(fname, 'r')\n",
    "    arraylist = []\n",
    "    for line in f:\n",
    "        linelist = line.strip().split()\n",
    "        for v in linelist:\n",
    "            dtype = np.int\n",
    "            if '.' in v:\n",
    "                dtype = np.float\n",
    "            arraylist.append(dtype(v))\n",
    "    f.close()\n",
    "    return np.reshape(np.array(arraylist[:ncol*nrow]), (nrow, ncol))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#read bottom.txt here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# answer for earlier sk test\n",
    "dt=[('col1', int), ('col2', 'S10'), ('col3', float), ('col4', 'S20')]\n",
    "myrecarray = np.zeros((100), dtype=dt).view(np.recarray)\n",
    "myrecarray.col1[:] = np.arange(99,-1, -1)\n",
    "myrecarray.col2[:] = 'row '\n",
    "myrecarray.col3[:] = np.random.random((100))\n",
    "myrecarray.col4[:] = 'cdl'\n",
    "print(myrecarray.dtype.names)\n",
    "print(myrecarray)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Raw Cell Format",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
