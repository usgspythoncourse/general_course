#General Python Introduction
The following is a tentative course agenda.  Please note that this is only a draft at this time and is subject to change.

### Tuesday, Nov 15, 2016
8:00 AM 	– 	Introductions and Getting Started MIKE  
8:30 AM 	– 	Python Overview MIKE  
9:15 AM 	– 	Python Basics MIKE  
10:00 AM 	– 	Break  
10:15 AM 	– 	Python Basics (cont.) MIKE  
11:00 AM	–	Functions and Scripts using PyCharm ANDY  
12:00 PM 	– 	Lunch  
1:00 PM 	– 	Objects MIKE  
2:15 PM 	– 	Working with Files ANDY  
3:30 PM	    –	Break  
3:45 PM 	– 	Numpy MIKE  
5:00 PM 	– 	Class Adjourn  

### Wednesday, Nov 16, 2016
8:00 AM 	– 	Matplotlib ANDY  
9:30 AM		–	Namespace, Modules, Packages MIKE    
10:00 AM	–	Break  
10:15 AM	–	Sys, Os, Shutil ANDY
11:00 AM	–	Pandas MIKE  
12:00 PM	–	Lunch  
1:00 PM		–	Pandas Continued MIKE  
3:00 PM		–	Lotke Volterra Project with Debugging and Classes MIKE  
5:00 PM		–	Adjourn  


### Thursday, Nov 17, 2016
8:00 AM	–	Python GIS stack intro  
8:30 AM -   Reading and Writing vector data  
9:00 AM - Shapley and Pyproj  
11:00 AM - Crime example (spatial joins, nearst neighbor analysis)  
12:00 PM	–	Lunch  
1:00 PM	–	Reading and Writing raster data  
1:30 PM	–	Clipping and Mosaicing  
2:00 PM	–	Resampling and reprojecting  
2:45 PM	–	Raster statistics  
3:30 PM -  Raster comparison (Lake Mendota volume example)  
4:00 PM	–	LiDAR point cloud interpolation (time allowing)  
5:00 PM	–	Class Adjourn


