#General Python Introduction--Menlo Park
#### Instructors: Mike Fienen and Andy Leaf, USGS Wisconsin Water Science Center

#### Dates: July 25--27, 2017 (July 28 morning optional)
#### Time: 9:00am -- 5:00 pm
#### Location: USGS Menlo Park Building 3 California Conference Room (Room 3-237) (call-in information will be provided for remote participants)
The following is a tentative course agenda. Times are not included but we strive to cover the topics of each day. Breaks and lunch times will be determined by the class.

### Tuesday, July 25, 2017 (Basics)
Introductions and Getting Started   **Mike**  
Python Overview   **Mike**  
Python Basics     **Mike**  
Functions and Scripts using PyCharm     **Andy**  
Objects and Object-Oriented Software Design **Mike**  
Working with Files   **Andy**  

* 	string formatting    

Numpy and Scipy **Mike**  

### Wednesday, July 26, 2017 (Numerics and Analysis)
Comparing Python with MATLAB  **Mike**  
Downflow Multivent KDE conversion MATLAB --> Python  **Mike**  
Matplotlib   **Andy**  
Namespace, Modules, Packages    **Andy**     

*	best import practices  
*	reference vs. values; making copies  
*	conventions  
	
Sys, Os, Shutil **Andy**  
Pandas   **Mike**  

### Thursday, July 27, 2017 (GIS and beyond)
Overview/Intro  **Andy**  
Python GIS stack intro  **Andy**  
Reading and Writing vector data  **Andy**  
Shapley and Pyproj  **Andy**  
Spatial joins, nearst neighbor analysis  **Andy**  
Reading and Writing raster data **Andy**  
Raster Operations **Andy**  
Wrap up **Mike**

### Friday, July 28, 2017 (Optional consulting)
Mike and Andy available for one-on-one consultation until noon