#General Python Introduction--Menlo Park
#### Instructors: Mike Fienen and Andy Leaf, USGS Wisconsin Water Science Center

#### Dates: July 25--27, 2017 (July 28 morning optional)
#### Time: 9:00am -- 5:00 pm
#### Location: USGS Menlo Park Building 3 California Conference Room (Room 3-237) (call-in information will be provided for remote participants)
The following is a tentative course agenda. Times are not included but we strive to cover the topics of each day. Breaks and lunch times will be determined by the class.

### Tuesday, July 25, 2017 (Basics)
Introductions and Getting Started  
Python Overview    
Python Basics     
Functions and Scripts using PyCharm       
Objects and Object-Oriented Software Design   
Working with Files     

* 	string formatting    

Numpy and Scipy   

### Wednesday, July 26, 2017 (Numerics and Analysis)
Comparing Python with MATLAB    
Downflow Multivent KDE conversion MATLAB --> Python    
Matplotlib     
Namespace, Modules, Packages         

*	best import practices  
*	reference vs. values; making copies  
*	conventions  
	
Sys, Os, Shutil   
Pandas     

### Thursday, July 27, 2017 (GIS and beyond)
Overview/Intro    
Python GIS stack intro    
Reading and Writing vector data    
Shapley and Pyproj    
Spatial joins, nearst neighbor analysis    
Reading and Writing raster data   
Raster Operations   
Wrap up 

### Friday, July 28, 2017 (Optional consulting)
Mike and Andy available for one-on-one consultation until noon